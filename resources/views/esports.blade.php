@extends('master')
@section('title', 'eSports')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2"><!-- left gutter --></div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8 embed-responsive-16by9"><iframe width="100%" height="458" src="https://www.youtube.com/embed/1nicXC3U2lQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe></div>
                            <div class="col-md-4" style="background-color: #ffffff; margin-left:-15px; margin-bottom:6px">Score ticker goes here.</div>
                        </div>
                    </div>
                    <div class="col-md-2"><!-- right gutter --></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-4">Twitter 1</div>
                    <div class="col-md-4">Twitter 2</div>
                    <div class="col-md-4">Twitter 3</div>
                </div>
            </div>
            <div class="col-md-1">
            </div>
        </div>
    </div>
@endsection