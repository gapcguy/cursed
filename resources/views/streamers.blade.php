@extends('master')
@section('title', 'Single Streamers')

@section('content')
<!-- Featured streamer section -->
<div class="jumbotron text-center" style="background-color:transparent">
		<h1 style="margin-bottom: 50px">Featured streamer</h1>
		<div class="row content">
			<div class="col-lg-2"></div>
			<div class="col-lg-8 embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="http://player.twitch.tv/?channel=GamesWithNix" width="100%" height="600px"></iframe>
			</div>
			<div class="col-lg-2"></div>
		</div>    
</div>
	<!-- end Featured streamer section -->
<div class="row">
	<div class="col-sm-12">
		<p>Live</p>
		<hr>
	</div>
	<div class="col-sm-12 text-center">
		<div class="row">
			<div class="col-sm-4">1</div>
			<div class="col-sm-4">2</div>
			<div class="col-sm-4">3</div>
		</div>
		<div class="row">
			<div class="col-sm-4">4</div>
			<div class="col-sm-4">5</div>
			<div class="col-sm-4">6</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<p>Other Member streams</p>
		<hr>
	</div>
	<div class="col-sm-12 text-center">
		<div class="row">
			<div class="col-sm-4">1</div>
			<div class="col-sm-4">2</div>
			<div class="col-sm-4">3</div>
		</div>
		<div class="row">
			<div class="col-sm-4">4</div>
			<div class="col-sm-4">5</div>
			<div class="col-sm-4">6</div>
		</div>	
	</div>
</div>
@endsection