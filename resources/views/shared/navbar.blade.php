<!-- nav section -->
<!-- Header section -->
<div class="masthead" style="background-color:rgba(0, 0, 0, 0.3);">
        <div class="soc-icons">
            <div class="row">
                <div class="col-lg-12" >
                        <a href="https://www.facebook.com/CRSDGamers/" class="mx-2" target="_blank" style="color:white"><i class="fab fa-facebook-f" style="font-size: 25px;"></i></a>
                        <a href="https://twitter.com/crsdgamers" class="mx-2" target="_blank" style="color:white"><i class="fab fa-twitter" style="font-size: 25px;"></i></a>
                        <a href="https://www.instagram.com/crsdgamers/?hl=en" class="mx-2" target="_blank"  style="color:white"><i class="fab fa-instagram" style="font-size: 25px;"></i></a>
                        <a href="https://www.youtube.com/channel/UCvGYbgv3BMctdowaFmZNh1A" class="mx-2" target="_blank" style="color:white"><i class="fab fa-youtube" style="font-size: 25px;"></i></a>
                </div>
            </div>
        </div>
    </div><br>&nbsp;
<header class="header-section">
    <div class="container">
        <ul class="main-menu-left site-menu-style">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/')}}">Home</a></li>
            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/about')}}">About</a></li>
            <li class="nav-item {{ Request::is('news') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/news')}}">News</a></li>
            <li>
                    <div class="dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Media</a>
                        <div class="dropdown-menu" style="background-color:#000000;">
                            <ul style="list-style-type: none; margin-left: 15px;">
                                <li><span class="nav-item {{ Request::is('videos') ? 'active' : '' }}"><a href="{{ url ('/videos')}}" class="nav-sub">Videos</a></span></li><br>
                                <li><span class="nav-item {{ Request::is('streamers') ? 'active' : '' }}"><a href="{{ url ('/streamers')}}" class="nav-sub">Streamers</a></span></li><br>
                                <li><span class="nav-item {{ Request::is('teams') ? 'active' : '' }}"><a href="{{ url ('/teams')}}" class="nav-sub">Teams</a></span></li><br>
                                <li><span class="nav-item {{ Request::is('esports') ? 'active' : '' }}"><a href="{{ url ('/esports')}}" class="nav-sub">eSports</a></span></li><br>
                                <hr style="height:2px; border:none; color:rgb(148,204,83); background-color:rgb(148,204,83);margin-right:10px;margin-left:-10px;">
                                <li><span class="nav-item {{ Request::is('tiktok') ? 'active' : '' }}"><a href="{{ url ('/tiktok')}}" class="nav-sub">TikTok</a></span></li>
                            </ul>
                        </div>
                    </div>
                </li>
        </ul>
        <a class="site-logo" href="/"><img src="img/cursedLogo.png" height="140px"></a>
        <ul class="main-menu-right site-menu-style">
            <li class="nav-item {{ Request::is('partners') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/partners')}}">Partners</a></li>
            <li class="nav-item {{ Request::is('membersonly') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/membersonly')}}">Members Only</a></li>
            <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}"><a class="nav-link" href="{{ url ('/contact')}}">Contact</a></li>
        </ul>
    </div>
</header>
<!-- Header section end -->

