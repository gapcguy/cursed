@extends('master')
@section('title', 'About Us')

@section('content')
<!-- About Section -->
<section id="about" class="about-section text-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 mx-auto" style="z-index:5">
          <h2 class="text-white mb-4" style="margin-top: -30px;">Entertainment and community for every player.</h2>
          <p style="color:#fff">Cursed is a video gaming website, community, and organization with a<br> focus on non-toxic and conscientious competitive and casual gaming.</p>
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto" style="margin-top:-90px;">
            <span style="margin-left:90%;"><img src="img/xbox-controller.png"></span>
            </div>
    </div>
      
    </div>
  </section>

  <!-- Projects Section -->
  <section id="projects" class="projects-section bg-light">
    <div class="container">

      <!-- Featured Project Row -->
      <div class="row align-items-center no-gutters mb-4 mb-lg-5">
        <div class="col-xl-8 col-lg-7">
          <img class="img-fluid mb-3 mb-lg-0" src="img/bg-masthead.jpg" alt="">
        </div>
        <div class="col-xl-4 col-lg-5">
          <div class="featured-text text-center text-lg-left">
            <h4>What we have in store</h4>
            <p class="text-black-50 mb-0">Get ready for an evolution in the video gaming community. With Cursed gamers, you become a part of a larger ecosphere in the fields of entertainment and eSports. Cursed Gamers' website and social platform will transform the way you game, the way you play, and how you live your life through gaming.</p>
          </div>
        </div>
      </div>

      
      <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
        <div class="col-lg-6">
          <img class="img-fluid grayscale" src="img/content-creator.jpg" alt="">
        </div>
        <div class="col-lg-6">
          <div class="bg-black text-center h-100 project" style="margin-bottom: -10px">
            <div class="d-flex h-100" style="margin-bottom: -12px;">
              <div class="project-text w-100 my-auto text-center text-lg-left">
                <h4 class="text-white" style="margin-top:-30px">Calling Content Creators</h4>
                <p class="mb-0 text-white-50">Cursed Gamers will not only give you a platform to share your experiences and your content through streaming, videos, and more; it will also help increase viewership and interaction, using Cursed Gamers as a catalyst to spread and grow your brand.<br>&nbsp;<br>&nbsp;<br>Your community deserves everything - they deserve to be <strong><i>Cursed.</i></strong></p>
                <hr class="d-none d-lg-block mb-0 ml-0">
              </div>
            </div>
          </div>
        </div>
      </div>

      
      <div class="row justify-content-center no-gutters">
        <div class="col-lg-6">
          <img class="img-fluid grayscale" src="img/controllers.jpg" alt="">
        </div>
        <div class="col-lg-6 order-lg-first">
          <div class="bg-black text-center h-100 project">
            <div class="d-flex h-100">
              <div class="project-text w-100 my-auto text-center text-lg-right">
                <h4 class="text-white">Community</h4>
                <p class="mb-0 text-white-50">Without community, we are but one person in front of a screen with a controller in hand. With community, we level up beyond that. Cursed will give the gaming community the power it deserves; supporting each gamer, and enabling them to be a part of something greater than themselves with our innovative structure and interactivity.</p>
                <hr class="d-none d-lg-block mb-0 mr-0">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
        <div class="col-lg-6">
          <img class="img-fluid grayscale" src="img/members.jpg" alt="">
        </div>
        <div class="col-lg-6">
          <div class="bg-black text-center h-100 project">
            <div class="d-flex h-100" style="margin-bottom: -11px;margin-top:-5px">
              <div class="project-text w-100 my-auto text-center text-lg-left">
                <h4 class="text-white">Entirely member-based</h4>
                <p class="mb-0 text-white-50">To truly reach your full potential as a Cursed Gamer, we have built a completely member-based system. Becoming a member gives you the necessary strength and privilege that being part of the general public cannot  with a proprietary and dynamic database to customize your experience and gain access to essential perks you never knew you wanted.</p>
                <hr class="d-none d-lg-block mb-0 ml-0">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center no-gutters" style="margin-top:-6px">
        <div class="col-lg-6">
          <img class="img-fluid grayscale" src="img/change-in-gaming.jpg" alt="">
        </div>
        <div class="col-lg-6 order-lg-first">
          <div class="bg-black text-center h-100 project">
            <div class="d-flex h-100" style="margin-bottom: -20px;">
              <div class="project-text w-100 my-auto text-center text-lg-right">
                <h4 class="text-white">Make Change in Gaming</h4>
                <p class="mb-0 text-white-50">Social gaming is subject to unfortunate culture and interactions. "Toxic gaming" has become a term we have gained by not taking the initiative and effort to respect that the person on the other end of that screen is a person as well. You can help change that culture! By being a part of the solution, and helping to make gaming enjoyable for everyone. Cursed Gamers will help enable you to do that.</p>
                <hr class="d-none d-lg-block mb-0 mr-0">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection