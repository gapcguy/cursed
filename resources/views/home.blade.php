@extends('master')
@section('title', 'Home')
@section('content')
<!-- Featured streamer section -->
<div class="jumbotron text-center" style="background-color:transparent">
    <h1 style="margin-bottom: 50px">Featured streamer</h1>
    <div class="row content">
        <div class="col-xl-2"></div>
        <div class="col-xl-8 embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="http://player.twitch.tv/?channel=GamesWithNix" width="100%" height="600px"></iframe>
        </div>
        <div class="col-xl-2"></div>
    </div>    
</div>
<!-- end Featured streamer section -->
<!-- begin secondary sections-->
<div class="row" style="margin-right:10px;margin-left:10px;">
    <div class="col-xl-6 text-center">
        <h2 style="color: #ffffff">Featured videos</h2>

    </div>
    <div class="col-xl-6 text-center">
        <h2 style="color: #ffffff">Gaming News</h2>
        <div class="row">
            <div class="col-xl-6">
                    @php
                    $feed = Feed::loadRSS('https://mynintendonews.com/feed?type=rss;action=.xml;limit=25');
                    $count = 0;
                    @endphp
            
            @foreach($feed->item as $item)
                @php
                    if($count == 1) break;
                @endphp
            <div class="card">
                    <p><a href="{{ $item->link }}">{{ $item->title }}</a></p>
                    <p>{{ $item ->description }}</p>
                </div>
                @php
                    $count++;
                @endphp
            @endforeach
            </div>
            <div class="col-xl-6">
                    @php
                    $feed = Feed::loadRSS('https://mynintendonews.com/feed?type=rss;action=.xml;limit=25');
                    $count = 0;
                    @endphp
            
            @foreach($feed->item as $item)
                @php
                    if($count == 1) break;
                @endphp
            <div class="card">
                    <p><a href="{{ $item->link }}">{{ $item->title }}</a></p>
                    <p>{{ $item ->description }}</p>
                </div>
                @php
                    $count++;
                @endphp
            @endforeach
            </div>
        </div>
    </div>
<!-- end secondary sections -->
</div>
<p>&nbsp;</p>
@endsection