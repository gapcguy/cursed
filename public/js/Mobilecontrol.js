 'use strict';

 (function($) {
     /*------------------
         Navigation
     --------------------*/
     $('.header-section .container').append('<div class="mobile-nav-switch"><i class="fa fa-bars"></i></div><ul class="mobile-menu"></ul>')
     var m1 = $('.main-menu-left ').children().clone();
     var m2 = $('.main-menu-right ').children().clone();
     $('.mobile-menu').append(m1,m2);
     $('.mobile-nav-switch').on('click', function () {
         $('.mobile-menu').slideToggle();
     });
 
  
 
     /*------------------
         Accordions
     --------------------*/
     $('.panel-header').on('click', function (e) {
         $('.panel-header').removeClass('active');
         var $this = $(this);
         if (!$this.hasClass('active')) {
             $this.addClass('active');
         }
         e.preventDefault();
     });
 
 })(jQuery);
 