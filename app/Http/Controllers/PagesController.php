<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {return view('home');}

    public function about()
    {return view('about');}

    public function news()
    {return view('news');}
    
    public function videos()
    {return view('videos');}

    public function streamers()
    {return view('streamers');}

    public function esports()
    {return view('esports');}

    public function tiktok()
    {return view('tiktok');}
    
    public function partners()
    {return view('partners');}
    
    public function membersonly()
    {return view('membersonly');}
        
    public function contact()
    {return view('contact');}
    
    public function teams()
    {return view('teams');}
}