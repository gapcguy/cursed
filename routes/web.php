<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/about', 'PagesController@about');
Route::get('/news', 'PagesController@news');
Route::get('/videos', 'PagesController@videos');
Route::get('/streamers', 'PagesController@streamers');
Route::get('/teams', 'PagesController@teams');
Route::get('/esports', 'PagesController@esports');
Route::get('/tiktok', 'PagesController@tiktok');
Route::get('/partners', 'PagesController@partners');
Route::get('/members-only', 'PagesController@membersonly');
Route::get('/contact', 'PagesController@contact');

Auth::routes();

Route::get('/panel', 'HomeController@index')->name('ahome');
